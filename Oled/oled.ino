/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void getClock(){
  printf("get clock");
}

void draw(){
  display.clearDisplay();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  // Display static text
  display.println("Hello, world!");
  display.display();
}

void setup()
{
  Serial.begin(115200);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;
  }
  delay(2000);
  
}

void loop()
{
  display.clearDisplay();
  for(int j = 0; j <128; j++){
    display.writePixel(j,0,1);
  }
  for(int j = 0; j <128; j++){
    display.writePixel(j,63,1);
  }
  for(int j = 0; j <64; j++){
    display.writePixel(0,j,1);
  }
  for(int j = 0; j <64; j++){
    display.writePixel(127,j,1);
  }

  // display.writePixel(2,0,1);
  // display.writePixel(100,0,1);
  // display.writePixel(100,63,1);
  display.display();
  delay(2000);
}