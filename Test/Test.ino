/*
Jam internet
esp32, ambil data tanggal dan waktu dari ntp server
tampilkan ke lcd
-manfaatkan multitasking (minimal 2 task)
    - task1 ambil data
    - task2 tampilkan ke lcd
-teknik task synchronization & communication dari freeRTOS
simpan dengan nama iclock_bernardinus_realino.ino
*/

#include <stdlib.h>
#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

TaskHandle_t Task1;
TaskHandle_t Task2;
SemaphoreHandle_t semaphore;

int counter = 0;

#define LED_PIN 5
#define SSID "Bernard"
#define PASS "romaroller"

#define NTPCLIENT

#ifdef NTPCLIENT
const long utcOffsetInSeconds = 25200;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);
#endif

void Task1code(void *pvParameters)
{
    // for (;;)
    // {
        xSemaphoreTake(semaphore, portMAX_DELAY);
        // xSemaphoreGive(semaphore);
        Serial.print("Task2 running on core ");
        Serial.println(xPortGetCoreID());
        display.clearDisplay();
        draw();
        display.setTextSize(2);
        display.setTextColor(WHITE);
        display.setCursor(15, 15);
        display.println(timeClient.getFormattedTime());
        display.setCursor(15, 35);
        display.println(daysOfTheWeek[timeClient.getDay()]);
        // display.println("hello world");
        display.display();
        // Serial.println(timeClient.getFormattedTime());
        Serial.println(counter);
        counter++;
    // }
}

//Task2code: blinks an LED every 700 ms
void Task2code(void *pvParameters)
{
    
    // for (;;)
    // {
        // xSemaphoreTake(semaphore, portMAX_DELAY);
        xSemaphoreGive(semaphore);
        Serial.print("Task1 running on core ");
        Serial.println(xPortGetCoreID());
        timeClient.update();
        Serial.println(counter);
        counter++;
        vTaskDelay(1000);
    // }
}

void draw()
{
    for (int j = 0; j < 128; j++)
    {
        display.writePixel(j, 0, 1);
    }
    for (int j = 0; j < 128; j++)
    {
        display.writePixel(j, 63, 1);
    }
    for (int j = 0; j < 64; j++)
    {
        display.writePixel(0, j, 1);
    }
    for (int j = 0; j < 64; j++)
    {
        display.writePixel(127, j, 1);
    }
}

void setup()
{
    Serial.begin(115200);
    pinMode(LED_PIN, OUTPUT);
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C); //Display
    //WiFi
    WiFi.begin(SSID, PASS);
    while (WiFi.status() != WL_CONNECTED)
    {
        display.clearDisplay();
        display.setTextSize(2);
        display.setTextColor(WHITE);
        display.setCursor(15, 15);
        display.printf(".");
        display.display();
        Serial.printf(".");
        delay(500);
    }
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(15, 15);
    display.printf("Connected");
    display.display();
    digitalWrite(LED_PIN, HIGH); //indicator
    timeClient.begin();          //Time NTP

    semaphore = xSemaphoreCreateMutex();
    xTaskCreatePinnedToCore(
        Task1code, /* Task function. */
        "Task1",   /* name of task. */
        10000,     /* Stack size of task */
        NULL,      /* parameter of the task */
        1,         /* priority of the task */
        &Task1,    /* Task handle to keep track of created task */
        0);        /* pin task to core 0 */
    delay(500);

    //create a task that will be executed in the Task2code() function, with priority 1 and executed on core 1
    xTaskCreatePinnedToCore(
        Task2code, /* Task function. */
        "Task2",   /* name of task. */
        10000,     /* Stack size of task */
        NULL,      /* parameter of the task */
        1,         /* priority of the task */
        &Task2,    /* Task handle to keep track of created task */
        1);        /* pin task to core 1 */
    delay(500);
}

void loop()
{
    delay(10);
}