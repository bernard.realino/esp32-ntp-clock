/*
Internet Clock
esp32, ambil data tanggal dan waktu dari ntp server
tampilkan ke lcd
-manfaatkan multitasking (minimal 2 task)
    - task1 ambil data
    - task2 tampilkan ke lcd
-teknik task synchronization & communication dari freeRTOS
simpan dengan nama iclock_bernardinus_realino.ino
*/

#include <stdlib.h>
#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define LED1 5
#define LED2 13
#define SSID "Bernard"
#define PASS "romaroller"

#define NTPCLIENT

#ifdef NTPCLIENT
const long utcOffsetInSeconds = 25200;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);
#endif

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

TaskHandle_t Task1, Task2;
SemaphoreHandle_t baton;

int counter = 0;

void blink(byte pin, int duration)
{

  digitalWrite(pin, HIGH);
  delay(duration);
  digitalWrite(pin, LOW);
  delay(duration);
}

//draw border
void draw()
{
    for (int j = 0; j < 128; j++)
    {
        display.writePixel(j, 0, 1);
    }
    for (int j = 0; j < 128; j++)
    {
        display.writePixel(j, 63, 1);
    }
    for (int j = 0; j < 64; j++)
    {
        display.writePixel(0, j, 1);
    }
    for (int j = 0; j < 64; j++)
    {
        display.writePixel(127, j, 1);
    }
}

void codeForTask1(void *parameter)
{
  for (;;)
  {
    xSemaphoreTake(baton, portMAX_DELAY);
    timeClient.update();
    xSemaphoreGive(baton);
    delay(50);
    Serial.print("Counter in Task 1: ");
    Serial.println(counter);
    counter++;
  }
}

void codeForTask2(void *parameter)
{
  for (;;)
  {
    xSemaphoreTake(baton, portMAX_DELAY);
    display.clearDisplay();
    draw();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(15, 15);
    display.println(timeClient.getFormattedTime());
    display.setCursor(15, 35);
    display.println(daysOfTheWeek[timeClient.getDay()]);
    display.display();
    xSemaphoreGive(baton);
    delay(50);
    Serial.print("                            Counter in Task 2: ");
    Serial.println(counter);
    counter++;
  }
}

void setup()
{
  Serial.begin(115200);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); //Display
  //WiFi
  WiFi.begin(SSID, PASS);
  int x = 15;
  while (WiFi.status() != WL_CONNECTED)
  {
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(x, 15);
    display.printf(".");
    display.display();
    Serial.printf(".");
    x += 4;
    delay(500);
  }
  timeClient.begin();
  delay(500);
  timeClient.update();
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(15, 15);
  display.printf("Connected");
  display.display();

  baton = xSemaphoreCreateMutex();

  xTaskCreatePinnedToCore(
      codeForTask1,
      "led1Task",
      1000,
      NULL,
      1,
      &Task1,
      0);
  delay(500); // needed to start-up task1

  xTaskCreatePinnedToCore(
      codeForTask2,
      "led2Task",
      1000,
      NULL,
      1,
      &Task2,
      1);
}

void loop()
{

  delay(10);
}