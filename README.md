# Esp32 NTP Clock

---Bernardinus Realino---

Internet Clock with ESP32 and OLED Using Multicore

Internet Clock
- Get date and time from NTP
- Display using OLED
- Multitasking (2 task):
    - Task 1: get clock data
    - Task 2: display to oled
- Synchronization & communication dari freeRTOS

Clock Photo and Video inside Photo and Video Folder

references:
- https://github.com/SensorsIot/ESP32-Dual-Core