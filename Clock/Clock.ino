/*
Jam internet
esp32, ambil data tanggal dan waktu dari ntp server
tampilkan ke lcd
-manfaatkan multitasking (minimal 2 task)
    - task1 ambil data
    - task2 tampilkan ke lcd
-teknik task synchronization & communication dari freeRTOS
simpan dengan nama iclock_bernardinus_realino.ino
*/

// #include <Wifi.h>
#include <stdlib.h>
#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>

#define LED_PIN 5
#define SSID "laptop"
#define PASS "qwer1234"

#define NTPCLIENT

#ifdef NTPCLIENT
const long utcOffsetInSeconds = 3600;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);
#endif

void setup()
{
    Serial.begin(115200);
    pinMode(LED_PIN, OUTPUT);
    WiFi.begin(SSID, PASS);
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.printf(".");
        delay(500);
    }
    Serial.println("Connected");
    digitalWrite(LED_PIN, HIGH);
    timeClient.begin();
}

void loop()
{
    timeClient.update();

    Serial.print(daysOfTheWeek[timeClient.getDay()]);
    Serial.print(", ");
    Serial.print(timeClient.getHours());
    Serial.print(":");
    Serial.print(timeClient.getMinutes());
    Serial.print(":");
    Serial.println(timeClient.getSeconds());
    Serial.println(timeClient.getFormattedTime());

    delay(1000);
}